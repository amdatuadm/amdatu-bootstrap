/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.webresources;

import java.io.File;
import java.io.FileWriter;
import java.net.URL;
import java.nio.file.Path;
import java.util.List;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.InstallResult.Builder;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.amdatu.services.WebResourcesService;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.amdatu.bootstrap.services.ResourceManager;
import org.amdatu.template.processor.TemplateContext;
import org.amdatu.template.processor.TemplateEngine;
import org.amdatu.template.processor.TemplateProcessor;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Inject;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.framework.BundleContext;

@Component
public class WebResourcesPlugin implements BootstrapPlugin{

	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile ResourceManager m_resourceManager;
	
	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile WebResourcesService m_webResourcesService;

	@ServiceDependency
	private volatile TemplateEngine m_templateEngine;

	@Inject
	private volatile BundleContext m_bundleContext;
	
	interface WebInstallArgs extends Parameters {
		@Description("Directory containing the static resources")
		@Required
		String dir();

		@Description("Create static resources dir")
		boolean createDir();
		
		@Required
		@Description("Path to register")
		String path();
		
		@Description("Name of the default page")
		String defaultpage();
		
		@Description("Create default page")
		boolean createDefaultPage();

		@Description("Path to the bnd file")
		@ProjectBndFile
		@Required
		File bndfile();
	}

	@Command(scope=Scope.PROJECT)
	@Description("Add Amdatu Web Resources bundle headers")
	public void install(WebInstallArgs args, Prompt prompt) {
		String dir = args.dir();
		String path = args.path();
		String defaultpage = args.defaultpage();
		Path bndFile = m_navigator.getCurrentDir().resolve(args.bndfile().toPath());
		
		m_webResourcesService.addWebResourceHeaders(path, defaultpage, dir, bndFile);
		
		// create resource dir if wanted
		if (args.createDir()) {
			File resources = new File(m_navigator.getProjectDir().toFile(), dir);
			if (resources.exists()) {
				prompt.println("Can't create resource dir (dir exists)");
			}
			else {
				if (!resources.mkdir()) {
					prompt.println("Can't create project dir");
				}
			}
		}

		// create default page if wanted
		if (args.createDefaultPage()) {
			File resources = new File(m_navigator.getProjectDir().toFile(), dir);
			if (!resources.exists()) {
				prompt.println("Can't create default page (resource dir does not exists)");
			}
			else {
				File page = new File(resources, defaultpage);
				try {
					if (!page.createNewFile()) {
						prompt.println("Can't create default page (file exists)");
					}
					else {
						String content = getDefaultPageContent();
						FileWriter writer = new FileWriter(page);
						writer.write(content);
						writer.close();
					}
				} catch (Exception e) {
					prompt.println("Can't create default page: " + e.getMessage());
				}
			}
		}
	}
	
	private String getDefaultPageContent() throws Exception {
		URL templateUri = m_bundleContext.getBundle().getEntry("/templates/defaultpage.vm");
		TemplateProcessor processor = m_templateEngine.createProcessor(templateUri);
		TemplateContext context = m_templateEngine.createContext();
		String template = processor.generateString(context);
		return template;
	}

	interface RestRunArguments extends Parameters {
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File runConfig();
	}
	
	@Command(scope=Scope.PROJECT)
	@Description("Add Amdatu Web Resources run dependencies")
	public InstallResult run(RestRunArguments args) {
		Builder builder = InstallResult.builder();
		List<Dependency> deps = Dependency.fromStrings(
				"org.apache.felix.http.jetty", 
				"org.apache.felix.http.whiteboard", 
				"org.amdatu.web.resourcehandler", 
				"org.apache.felix.http.servlet-api",
				"org.apache.felix.http.api");
		
		Path path = args.runConfig().toPath();
		builder.addResult(m_dependencyBuilder.addRunDependency(deps, path));
		builder.addResult(m_dmService.addRunDependencies(path));
		
		return builder.build();
	}

	@Override
	public String getName() {
		return "web";
	}

}
