/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.amdatu.webresources;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.amdatu.bootstrap.plugins.amdatu.services.WebResourcesService;
import org.amdatu.bootstrap.services.ResourceManager;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

import aQute.bnd.build.model.BndEditModel;
import aQute.bnd.properties.Document;

@Component
public class WebRsourcesServiceImpl implements WebResourcesService{

	@ServiceDependency
	private volatile ResourceManager m_resourceManager;

	
	@Override
	public void addWebResourceHeaders(String path, String defaultPath, String resourcesDir, Path bndFile) {
		if (!path.startsWith("/")) {
			path = "/" + path;
		}

		try {
			BndEditModel model = new BndEditModel();
			model.loadFrom(bndFile.toFile());

			model.addIncludeResource(resourcesDir + "=" + resourcesDir);
			model.genericSet("X-Web-Resource-Version", " 1.1");

			model.genericSet("X-Web-Resource", createWebResourceHeader(resourcesDir, path, model));

			if (defaultPath != null && !defaultPath.equals("")) {
				model.genericSet("X-Web-Resource-Default-Page", " " + defaultPath);
			}

			String bndContents = new String(Files.readAllBytes(bndFile));
			Document document = new Document(bndContents);
			model.saveChangesTo(document);

			m_resourceManager.writeFile(bndFile, document.get().getBytes());
		} catch (IOException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private String createWebResourceHeader(String resourcesDir, String path, BndEditModel model) {
		Object webResource = model.genericGet("X-Web-Resource");

		StringBuilder webResourceBuilder = new StringBuilder(" ");
		if (webResource != null) {
			webResourceBuilder.append(webResource).append(";");
		}

		webResourceBuilder.append(path);
		webResourceBuilder.append(";");
		webResourceBuilder.append(resourcesDir);
		return webResourceBuilder.toString();
	}

}
