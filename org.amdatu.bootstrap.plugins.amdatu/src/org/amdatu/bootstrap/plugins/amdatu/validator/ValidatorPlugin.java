package org.amdatu.bootstrap.plugins.amdatu.validator;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class ValidatorPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DependencyBuilder m_dependencies;
	
	@ServiceDependency
	private volatile DmService m_dmService;
	
	interface RunParams extends Parameters{
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File bndRunFile();
	}
	
	@Command
	@Description("Add Validator run dependencies")
	public InstallResult install(RunParams params) {
		
		return m_dependencies.addDependencies(
				Dependency.fromStrings(
						"org.amdatu.validator", 
						"javax.validation.api"));
	}
	
	@Command
	@Description("Add Validator run dependencies")
	public InstallResult run(RunParams params) {
		InstallResult result1 = m_dmService.addRunDependencies(params.bndRunFile().toPath());
		
		InstallResult result2 = m_dependencies.addRunDependency(
				Dependency.fromStrings(
						"org.amdatu.validator", 
						"javax.validation.api", 
						"javax.el-api",
						"org.glassfish.web.javax.el"), params.bndRunFile().toPath());
		
		return InstallResult.builder().addResult(result1).addResult(result2).build();
	}
	
	@Override
	public String getName() {
		return "validator";
	}

}
