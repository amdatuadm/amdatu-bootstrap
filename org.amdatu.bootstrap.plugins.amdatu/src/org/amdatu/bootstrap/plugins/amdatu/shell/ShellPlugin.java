package org.amdatu.bootstrap.plugins.amdatu.shell;

import java.io.File;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class ShellPlugin implements BootstrapPlugin{
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	interface RunParams extends Parameters{
		@Description("Location of run configuration")
		@RunConfig
		File runFile();
	}
	
	@Command
	public InstallResult run(RunParams args) {
		return m_dependencyBuilder.addRunDependency(
				Dependency.fromStrings(
						"org.apache.felix.gogo.command",
						"org.apache.felix.gogo.runtime",
						"org.apache.felix.gogo.shell",
						"org.apache.felix.dependencymanager.shell"), args.runFile().toPath());
	}
	
	@Override
	public String getName() {
		return "shell";
	}

}
