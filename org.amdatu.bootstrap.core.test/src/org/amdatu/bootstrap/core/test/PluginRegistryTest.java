/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.amdatu.testing.configurator.TestConfigurator.*;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import org.amdatu.bootstrap.command.ProjectBndFile;
import org.amdatu.bootstrap.command.WorkspaceBundleSelect;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.core.CommandArgumentDescription;
import org.amdatu.bootstrap.core.CommandDescription;
import org.amdatu.bootstrap.core.EnumValue;
import org.amdatu.bootstrap.core.PluginDescription;
import org.amdatu.bootstrap.core.PluginRegistry;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.FrameworkUtil;


public class PluginRegistryTest {
	private final static int NR_OF_CORE_COMPONENTS = 1;
	private static final int NR_OF_EXTRA_COMPONENTS = 1;
	private static final int NR_OF_COMMANDS_ON_EXAMPLE = 5;
	
	private volatile PluginRegistry m_registry;
	private volatile BundleContext m_bundleContext = FrameworkUtil.getBundle(this.getClass()).getBundleContext();
	
	@Before
	public void setup() {
		
		
		configure(this).add(createServiceDependency().setService(PluginRegistry.class).setRequired(true)).apply();
		
		startComponentBundle();
	}
	
	@Test
	public void plugins_should_be_listed() {
		List<PluginDescription> plugins = m_registry.listPlugins();
		assertEquals(NR_OF_CORE_COMPONENTS + NR_OF_EXTRA_COMPONENTS, plugins.size());
	}
	
	@Test
	public void plugin_should_deregister() {
		stopComponentsBundle();
		
		List<PluginDescription> plugins = m_registry.listPlugins();
		assertEquals(NR_OF_CORE_COMPONENTS, plugins.size());
	}
	
	@Test
	public void plugin_should_register() {
		stopComponentsBundle();
		
		List<PluginDescription> plugins = m_registry.listPlugins();
		assertEquals(NR_OF_CORE_COMPONENTS, plugins.size());
		
		startComponentBundle();
		
		plugins = m_registry.listPlugins();
		assertEquals(NR_OF_CORE_COMPONENTS + NR_OF_EXTRA_COMPONENTS, plugins.size());
	}
	
	@Test
	public void plugin_should_be_retrievalbe() {
		Optional<BootstrapPlugin> plugin = m_registry.getPlugin("example");
		assertTrue(plugin.isPresent());
	}
	
	@Test
	public void pluginDescription_should_be_retrievalbe() {
		Optional<PluginDescription> plugin = m_registry.getPluginDescription("example");
		assertTrue(plugin.isPresent());
	}
	
	@Test
	public void plugin_should_have_commands() {
		Optional<PluginDescription> pluginDescription = m_registry.getPluginDescription("example");
		List<CommandDescription> commandDescription = pluginDescription.get().getCommandDescription();
		assertEquals(NR_OF_COMMANDS_ON_EXAMPLE, commandDescription.size());
	}
	
	@Test
	public void pluginDescription_should_list_basic_params() {
		Optional<PluginDescription> pluginDescription = m_registry.getPluginDescription("example");
		Optional<CommandDescription> toUpperCommand = pluginDescription.get().getCommandDescription("toUpperCommand");
		CommandArgumentDescription commandArgumentDescription = toUpperCommand.get().getArguments().get(0);
		
		assertEquals("arg0", commandArgumentDescription.getName());
		assertEquals("java.lang.String", commandArgumentDescription.getType());
	}
	
	@Test
	public void pluginDescription_should_list_interface_params() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("withoutDescription", getArgument(withInterace.get(), "withoutDescription").getName());
		assertEquals("withDescription", getArgument(withInterace.get(), "withDescription").getName());
		assertEquals("required", getArgument(withInterace.get(), "required").getName());
	}
	
	@Test
	public void pluginDescription_should_have_interface_types() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals(String.class.getName(), getArgument(withInterace.get(), "withoutDescription").getType());
		assertEquals(File.class.getName(), getArgument(withInterace.get(), "normalFile").getType());
	}
	
	@Test
	public void should_show_required_status() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertTrue(getArgument(withInterace.get(), "required").isRequired());
	}
	
	@Test
	public void arg_by_default_not_required() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertFalse(getArgument(withInterace.get(), "withoutDescription").isRequired());
	}
	
	@Test
	public void should_show_description() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("Example arg", getArgument(withInterace.get(), "withDescription").getDescription());
	}
	
	@Test
	public void should_have_default_description_with_name() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("withoutDescription", getArgument(withInterace.get(), "withoutDescription").getDescription());
	}
	
	@Test
	public void should_set_callback_for_dynamicselect() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("getListOfOptions", getArgument(withInterace.get(), "dynamicSelect").getCallback());
	}
	
	@Test
	public void should_set_enum_as_type_for_dynamicselect() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("enum", getArgument(withInterace.get(), "dynamicSelect").getType());
	}
	
	@Test
	public void should_set_enum_as_type_for_select() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("enum", getArgument(withInterace.get(), "staticStringOptions").getType());
	}
	
	@Test
	public void should_set_options_for_select() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		List<EnumValue> options = getArgument(withInterace.get(), "staticStringOptions").getOptions();
		assertEquals(3, options.size());
		
		assertEquals("A", options.get(0).getName());
		assertEquals("Option A", options.get(0).getDescription());
		
		assertEquals("B", options.get(1).getName());
		assertEquals("Option B", options.get(1).getDescription());
		
		assertEquals("C", options.get(2).getName());
		assertEquals("Option C", options.get(2).getDescription());
	}
	
	@Test
	public void should_set_enum_as_type_for_runconfig() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals("enum", getArgument(withInterace.get(), "bndRunFile").getType());
	}
	
	@Test
	public void should_set_callback_for_runconfig() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals(RunConfig.class.getName(), getArgument(withInterace.get(), "bndRunFile").getCallback());
	}
	
	@Test
	public void should_set_callback_for_bundleselect() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals(WorkspaceBundleSelect.class.getName(), getArgument(withInterace.get(), "bundle").getCallback());
	}
	
	@Test
	public void should_set_callback_for_projectbndfile() {
		Optional<CommandDescription> withInterace = getWithInterfaceCommand();
		
		assertEquals(ProjectBndFile.class.getName(), getArgument(withInterace.get(), "bndFile").getCallback());
	}
	
	@Test
	public void should_not_send_promt_param() {
		Optional<PluginDescription> pluginDescription = m_registry.getPluginDescription("example");
		Optional<CommandDescription> voidCommand = pluginDescription.get().getCommandDescription("voidCommand");
		
		assertEquals(0,voidCommand.get().getArguments().size());
	}

	private Optional<CommandDescription> getWithInterfaceCommand() {
		Optional<PluginDescription> pluginDescription = m_registry.getPluginDescription("example");
		Optional<CommandDescription> withInterace = pluginDescription.get().getCommandDescription("withInterface");
		return withInterace;
	}

	private CommandArgumentDescription getArgument(CommandDescription command, String name) {
		if(!command.getArgument(name).isPresent()) {
			fail("Command argument '" + "withoutDescription" + "' not found");
		}
		
		return command.getArgument(name).get();
	}

	private void startComponentBundle() {
		Optional<Bundle> componentsBundle = findComponentsBundle();

		try {
			componentsBundle.get().start();
		} catch (BundleException e) {
			e.printStackTrace();
		}
		
	}

	private Optional<Bundle> findComponentsBundle() {
		Optional<Bundle> componentsBundle = Arrays.stream(m_bundleContext.getBundles())
				.filter(b -> b.getSymbolicName().equals("org.amdatu.bootstrap.core.test.components"))
				.findAny();
		return componentsBundle;
	}

	private void stopComponentsBundle() {
		Optional<Bundle> componentsBundle = findComponentsBundle();
		
		try {
			componentsBundle.get().stop();
		} catch (BundleException e) {
			e.printStackTrace();
		}
	}
	
	@After
	public void after() {
		cleanUp(this);
	}
}
