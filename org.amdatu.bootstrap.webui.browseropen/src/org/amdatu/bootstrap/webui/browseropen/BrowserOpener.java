/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.webui.browseropen;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import org.amdatu.bootstrap.webui.WebBackendConfig;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;

@Component
public class BrowserOpener {

	@ServiceDependency
	private volatile WebBackendConfig m_config;
	
	@Start
	public void start() throws IOException, URISyntaxException {
		Desktop.getDesktop().browse(new URI("http://localhost:" + m_config.getPort() + "/ui/index.html"));
	}
}
