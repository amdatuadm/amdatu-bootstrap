interface CommandArgument {
    name : string;
    type : string;
    description : string;
    store : boolean;
}
