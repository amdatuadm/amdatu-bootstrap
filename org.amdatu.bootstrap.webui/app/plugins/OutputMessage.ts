class OutputMessage {
  message : string;
  level : number;

  constructor(message : string, level? : number) {
    this.message = message;

    if(!level) {
      this.level = 2;
    } else {
      this.level = level;
    }
  }

}

export = OutputMessage
