/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.util.List;
import java.util.Optional;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public class PluginDescription {
	private String m_name;
	private List<CommandDescription> m_commandDescription;
	
	public PluginDescription() {
	}
	
	public PluginDescription(String name, List<CommandDescription> commandDescription) {
		m_name = name;
		m_commandDescription = commandDescription;
	}

	public String getName() {
		return m_name;
	}

	public List<CommandDescription> getCommandDescription() {
		return m_commandDescription;
	}
	
	public Optional<CommandDescription> getCommandDescription(String commandName) {
		return m_commandDescription
				.stream()
				.filter(c -> c.getName().equals(commandName))
				.findAny();
	}

	public void setName(String name) {
		m_name = name;
	}

	public void setCommandDescription(List<CommandDescription> commandDescription) {
		m_commandDescription = commandDescription;
	}
}
