/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.core;

import java.util.List;
import java.util.Optional;

import org.amdatu.bootstrap.command.Scope;

import aQute.bnd.annotation.ProviderType;

@ProviderType
public class CommandDescription {
	private String m_name;
	private String m_description;
	private List<CommandArgumentDescription> m_arguments;
	private String m_scope;

	public CommandDescription() {
	}
	
	public CommandDescription(String name, String description, Scope scope, List<CommandArgumentDescription> arguments) {
		m_name = name;
		m_description = description;
		m_scope = scope.toString();
		m_arguments = arguments;
	}

	public String getName() {
		return m_name;
	}

	public String getDescription() {
		return m_description;
	}

	public List<CommandArgumentDescription> getArguments() {
		return m_arguments;
	}
	
	public Optional<CommandArgumentDescription> getArgument(String name) {
		return m_arguments.stream().filter(a -> a.getName().equals(name)).findAny();
	}

	public String getScope() {
		return m_scope;
	}

	public void setName(String name) {
		m_name = name;
	}

	public void setDescription(String description) {
		m_description = description;
	}

	public void setArguments(List<CommandArgumentDescription> arguments) {
		m_arguments = arguments;
	}

	public void setScope(String scope) {
		m_scope = scope;
	}
}
