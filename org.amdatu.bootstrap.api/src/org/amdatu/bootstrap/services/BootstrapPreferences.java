package org.amdatu.bootstrap.services;

import java.util.ArrayList;
import java.util.List;

public class BootstrapPreferences {
	private String m_currentDir;
	private List<String> m_recentWorkspaces = new ArrayList<>();

	public String getCurrentDir() {
		return m_currentDir;
	}

	public void setCurrentDir(String currentDir) {
		m_currentDir = currentDir;
	}

	public List<String> getRecentWorkspaces() {
		return m_recentWorkspaces;
	}

	public void setRecentWorkspaces(List<String> recentWorkspaces) {
		m_recentWorkspaces = recentWorkspaces;
	}

}
