/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

public class CommandResult {
	private final String m_commandName;
	private final Object m_result;
	private final boolean m_successful;

	public CommandResult(String commandName, Object result, boolean successful) {
		m_commandName = commandName;
		m_result = result;
		m_successful = successful;
	}

	public CommandResult(String commandName, Object result) {
		m_commandName = commandName;
		m_result = result;
		m_successful = true;
	}

	public String getCommandName() {
		return m_commandName;
	}

	public Object getResult() {
		return m_result;
	}

	public boolean isSuccessful() {
		return m_successful;
	}
}
