/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.ws;

import com.fasterxml.jackson.annotation.JsonRawValue;


public class BootstrapMessage {
	private String m_type;
	
	
	private String m_body;

	public BootstrapMessage() {
	}

	public BootstrapMessage(String type, String body) {
		m_type = type;
		m_body = body;
	}
	
	public String getType() {
		return m_type;
	}

	public void setType(String type) {
		m_type = type;
	}

	@JsonRawValue
	public String getBody() {
		return m_body;
	}

	public void setBody(String body) {
		m_body = body;
	}

	@Override
	public String toString() {
		return "BootstrapMessage [m_type=" + m_type + ", m_body=" + m_body + "]";
	}
}
