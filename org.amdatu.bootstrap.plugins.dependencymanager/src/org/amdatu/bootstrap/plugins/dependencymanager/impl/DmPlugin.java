/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.plugins.dependencymanager.impl;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Description;
import org.amdatu.bootstrap.command.InstallResult;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.command.Required;
import org.amdatu.bootstrap.command.RunConfig;
import org.amdatu.bootstrap.command.Scope;
import org.amdatu.bootstrap.command.Select;
import org.amdatu.bootstrap.command.SelectOption;
import org.amdatu.bootstrap.command.StoreValue;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.amdatu.bootstrap.plugins.dependencymanager.DmService;
import org.amdatu.bootstrap.plugins.dependencymanager.DmVersion;
import org.amdatu.bootstrap.services.Dependency;
import org.amdatu.bootstrap.services.DependencyBuilder;
import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.Prompt;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;

@Component
public class DmPlugin implements BootstrapPlugin {

	@ServiceDependency
	private volatile DmService m_dmService;
	
	@ServiceDependency
	private volatile Navigator m_navigator;
	
	@ServiceDependency
	private volatile DependencyBuilder m_dependencyBuilder;
	
	interface DmInstallParameters extends Parameters {
		@StoreValue
		boolean useAnnotations();
		
		@Description("Dependency Manager version")
		@Select({@SelectOption(value="DM4", description="Dependency Manager 4"),
					@SelectOption(value="DM3", description="Dependency Manager 3")})
		String version();
	}
	
	@Command(scope=Scope.PROJECT)
	@Description("Install Apache Felix DependencyManager build dependencies and annotation processor")
	public InstallResult install(DmInstallParameters params) {
		boolean useAnnotations = params.useAnnotations();
		
		InstallResult annotationsInstallResult = null;
		if(useAnnotations) {
			annotationsInstallResult = m_dmService.installAnnotationProcessor(DmVersion.valueOf(params.version()));
		}
		
		return InstallResult.builder()
				.addResult(annotationsInstallResult)
				.addResult(m_dmService.addDependencies(DmVersion.valueOf(params.version()))).build();
	}
	
	interface DmRunParameters extends Parameters {
		@Required
		@RunConfig
		@Description("Run configuration to add dependencies to")
		File runConfig();
		
		@Description("Dependency Manager version")
		@Select({@SelectOption(value="DM4", description="Dependency Manager 4"),
					@SelectOption(value="DM3", description="Dependency Manager 3")})
		String version();
	}
	
	@Command(scope=Scope.PROJECT)
	@Description("Install Apache Felix DependencyManager run dependencies")
	public InstallResult run(DmRunParameters params) {
		return m_dmService.addRunDependencies(DmVersion.valueOf(params.version()), params.runConfig().toPath());
	}
	
	@Command(scope=Scope.WORKSPACE)
	@Description("Update Apache Felix DependencyManager 3.x to 4 in the workspace")
	public List<String> update(Prompt prompt) {
		List<String> changeLog = new ArrayList<>();
		
		Path processorJar = m_navigator.getWorkspaceDir().resolve("cnf/plugins/" + DmServiceImpl.ANNOTATION_PROCESSOR_JAR_DM3);
		try {
			if(processorJar.toFile().exists()) {
				Files.delete(processorJar);
			}
		} catch (IOException e) {
			prompt.println("Error updating DM annotation processor jar");
		}

		m_navigator.getProjectsInWorkspace().stream()
			.filter(p -> m_dependencyBuilder.hasDependency(new Dependency(".*/cnf/plugins/org.apache.felix.dependencymanager.annotation-3.*\\.jar"), p.getFile("bnd.bnd").toPath()))
			.forEach(p -> {
				
				Path bndFile = p.getFile("bnd.bnd").toPath();
				try {
					List<String> filteredLines = Files.readAllLines(bndFile).stream().filter(line -> !line.contains("org.apache.felix.dependencymanager.annotation")).collect(Collectors.toList());
					Files.write(bndFile, filteredLines, StandardOpenOption.TRUNCATE_EXISTING);
				} catch (Exception e) {
					prompt.println("Error updating DM in project " + p.getName());
				}
				
				
				m_dmService.installAnnotationProcessor(DmVersion.DM4, bndFile);
				changeLog.add("Updated annotation processor in project " + p.getBase().getName());
			});
		
		return changeLog;
	}

	@Override
	public String getName() {
		return "dm";
	}
}
