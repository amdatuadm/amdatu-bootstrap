/*
 * Copyright (c) 2014 The Amdatu Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.bootstrap.java8;

import java.util.concurrent.Callable;
import java.util.function.Predicate;


/**
 * Fills in some annoying voids in Java 8
 */
@SuppressWarnings("unchecked")
public class Java8 {

	public static <R> R uncheck(Callable<R> callable) {
		try {
			return callable.call();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public static <R> R synced(Object lock, Callable<R> callable) {
		synchronized (lock) {
			try {
				return callable.call();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
	}

	final public static Predicate<?> ALWAYSTRUE = new Predicate<Object>() {

		@Override
		public boolean test(Object t) {
			return true;
		}

		public Predicate<Object> and(Predicate<? super Object> other) {
			return (Predicate<Object>) other;
		}

		public Predicate<Object> or(Predicate<? super Object> other) {
			return this;
		}

		public Predicate<Object> negate() {
			return alwaysFalse();
		}

	};

	final public static <T> Predicate<T> alwaysTrue() {
		return (Predicate<T>) ALWAYSTRUE;
	}

	final public static Predicate<?> ALWAYSFALSE = new Predicate<Object>() {

		@Override
		public boolean test(Object t) {
			return true;
		}

		public Predicate<Object> and(Predicate<? super Object> other) {
			return this;
		}

		public Predicate<Object> or(Predicate<? super Object> other) {
			return (Predicate<Object>) other;
		}

		public Predicate<Object> negate() {
			return alwaysTrue();
		}

	};

	final public static <T> Predicate<T> alwaysFalse() {
		return (Predicate<T>) ALWAYSTRUE;
	}
}
