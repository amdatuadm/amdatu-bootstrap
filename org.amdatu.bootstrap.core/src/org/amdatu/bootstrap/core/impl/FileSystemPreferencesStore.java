package org.amdatu.bootstrap.core.impl;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import org.amdatu.bootstrap.services.BootstrapPreferences;
import org.amdatu.bootstrap.services.PreferencesStore;
import org.apache.commons.io.FileUtils;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.log.LogService;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class FileSystemPreferencesStore implements PreferencesStore{
	private final Path bootstrapDir = FileUtils.getUserDirectory().toPath().resolve(".amdatu-bootstrap");
	private final Path settingsFile = bootstrapDir.resolve("settings.json");
	private volatile BootstrapPreferences m_preferences;
	
	@ServiceDependency
	private volatile LogService m_logservice;
	
	@Start
	public void start() {
		if(!Files.exists(bootstrapDir, LinkOption.NOFOLLOW_LINKS)) {
			try {
				FileUtils.forceMkdir(bootstrapDir.toFile());
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	
	@Override
	public BootstrapPreferences get() {
		if(m_preferences != null) {
			return m_preferences;
		} else {
			try {
				ObjectMapper mapper = new ObjectMapper();
				m_preferences = mapper.readValue(settingsFile.toFile(), BootstrapPreferences.class);
				return m_preferences;
			} catch (IOException e) {
				m_logservice.log(LogService.LOG_WARNING, "Could not read properties file");
				return new BootstrapPreferences();
			}
		}
	}

	@Override
	public void store() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			mapper.writeValue(settingsFile.toFile(), get());
		} catch (IOException e) {
			m_logservice.log(LogService.LOG_ERROR, "Could not write settings file", e);
		}
	}

}
