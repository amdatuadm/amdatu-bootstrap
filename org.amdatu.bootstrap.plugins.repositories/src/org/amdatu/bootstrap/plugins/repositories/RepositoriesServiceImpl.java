package org.amdatu.bootstrap.plugins.repositories;

import static org.amdatu.bootstrap.java8.Java8.uncheck;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.amdatu.bootstrap.services.Navigator;
import org.amdatu.bootstrap.services.repositories.DependencyFilter;
import org.amdatu.bootstrap.services.repositories.DependencyType;
import org.amdatu.bootstrap.services.repositories.RepositoriesService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AbstractFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.indexer.ResourceIndexer;

import aQute.bnd.build.Container;
import aQute.bnd.build.Project;
import aQute.bnd.osgi.Builder;

@Component
public class RepositoriesServiceImpl implements RepositoriesService{

	@ServiceDependency 
	private ResourceIndexer m_indexer;
	
	@ServiceDependency
	private Navigator m_navigator;
	
	@Override
	public void materialize(File bndFile, Path target, DependencyType dependencyTypes, DependencyFilter filter) {
		try {
			try(Project project = new Project(m_navigator.getCurrentWorkspace(), bndFile.getParentFile(), bndFile)) {
				Collection<Container> bundles;
				
				if(dependencyTypes == null) {
					dependencyTypes = DependencyType.ALL;
				}
				
				switch (dependencyTypes) {
				case RUN:
					bundles = project.getRunbundles();
					break;
				case BUILD:
					bundles = project.getBuildpath();
					break;
				default:
					bundles = project.getRunbundles();
					bundles.addAll(project.getBuildpath());
					break;
				}
				
				Set<String> workspaceBundles = getWorkspaceBundles();
				
				Set<File> files = bundles.stream()
						.filter(b -> filterBundle(b, filter, workspaceBundles))
						.map(Container::getFile)
						.collect(Collectors.toSet());
				
				File outputDir = target.toFile();
				if(!outputDir.exists()) {
					outputDir.mkdirs();
				}
				
				try(FileOutputStream out = new FileOutputStream(new File(outputDir, "index.xml.gz"))) {
					try {
						files.forEach(f -> uncheck(() -> Files.copy(f.toPath(), outputDir.toPath().resolve(f.getName()))));

					} catch (Exception e) {
						throw new RuntimeException(e);
					}

					Set<File> newFileNames = files.stream().map(f -> new File(outputDir, f.getName())).collect(Collectors.toSet());
					Map<String, String> config = getConfig(outputDir);
					
					m_indexer.index(newFileNames, out, config);
				}
				
			}
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		
	}

	private Map<String, String> getConfig(File outputDir) {
		Map<String, String> config = new HashMap<String, String>();
		config.put(ResourceIndexer.ROOT_URL, outputDir.getAbsoluteFile().toURI().toString());
		return config;
	}

	private Set<String> getWorkspaceBundles() throws Exception {
		Set<String> workspaceBundles = m_navigator.getCurrentWorkspace().getAllProjects().stream()
				.flatMap(p -> uncheck(() -> p.getSubBuilders().stream()))
				.map(Builder::getBsn)
				.collect(Collectors.toSet());
		return workspaceBundles;
	}
	
	@Override
	public void materializeWorkspace(Path workspaceDir, File outputDir)  {
		
		if(!outputDir.exists()) {
			try {
				Files.createDirectories(outputDir.toPath());
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		IOFileFilter generatedJarFilter  = new AbstractFileFilter() {
			@Override
			public boolean accept(File pathname) {
				return pathname.getParentFile().getName().equals("generated") && pathname.getName().endsWith(".jar"); 
			}
		};

		Collection<File> jars = FileUtils.listFiles(workspaceDir.toFile(),  generatedJarFilter, TrueFileFilter.INSTANCE );
		
		Set<File> files = new HashSet<>();
		for (File file : jars) {
			try {
				FileUtils.copyFileToDirectory(file, outputDir);
				files.add(new File(outputDir, file.getName()));
			} catch (IOException e) {
				throw new RuntimeException(e);
			}
		}
		
		try(FileOutputStream out = new FileOutputStream(new File(outputDir, "index.xml.gz"))) {
			m_indexer.index(files, out, getConfig(outputDir));
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
			
	}
	
	private boolean filterBundle(Container b, DependencyFilter filter, Set<String> workspaceBundles) {
		String bsn = b.getBundleSymbolicName().replace(".jar", "");
		
		if(filter == DependencyFilter.NONE) {
			return true;
		} else if(filter == DependencyFilter.NON_WORKSPACE_ONLY) {
			boolean result = !workspaceBundles.contains(bsn);
			System.out.println(result + " for " + bsn);
			return result;
		} else {
			return workspaceBundles.contains(bsn);
		}
	}

	@Override
	public void materialize(File bndFile, Path target, DependencyType dependencyTypes) {
		materialize(bndFile, target, dependencyTypes, DependencyFilter.NONE);
	}

}
